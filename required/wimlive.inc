<?php
/**
  * @file
  * This file is use for congirure a live event.
  *
  */

//LIVE VIDEO
function wimtvpro_wimlive_output() {
  //View list future event created
  $output = l(t("Add") . " " . t("new event"), "admin/config/wimtvpro/wimlive/insert");
  $elenco = wimtvpro_elencoLive("all", "table");
  if (strpos($elenco, "<td>")) {
    $output .= "<table>";
    $output .= "<thead><tr><th>Name</th><th>Pay-Per-View</th><th>URL</th><th>Streaming</th><th>Embed Code</th><th></th></tr></thead>";
    $output .= "<tbody>";
    $output .= $elenco;
    $output .= "</tbody>";
    $output .= "</table>";
  }
  else {
    $output .= "<br/>" . $elenco;
  }
  return $output;
}

//Form for add new event live
function wimtvpro_wimlive_form($form_state) {
  return wimtvpro_form("insert", "");
}

//Form for modify event live
function wimtvpro_wimlive_formModify($form_state, $id) {
  return wimtvpro_form("modify", $id['build_info']['args'][0]);
}

//Call a delete event live
function wimtvpro_wimlive_delete($form_state, $id) {
  $identifier = $id['build_info']['args'][0];
  $userpeer = variable_get("userWimtv");
  $url_live = variable_get("basePathWimtv") . "liveStream/" . $userpeer . "/" . $userpeer . "/hosts";
  $url_live .= "/" . $identifier;
  $credential = variable_get("userWimtv") . ":" . variable_get("passWimtv");
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url_live);
  curl_setopt($ch, CURLOPT_VERBOSE, 0);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_USERPWD, $credential);
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  $response = curl_exec($ch);
  curl_close($ch);
  header('Location: admin/config/wimtvpro/wimlive/');
}

//This is a form
function wimtvpro_form($type, $identifier) {
  if ($type=="modify") {
    $userpeer = variable_get("userWimtv");
    $credential = variable_get("userWimtv") . ":" . variable_get("passWimtv");
    $url_live = variable_get("basePathWimtv") . "liveStream/" . $userpeer . "/" . $userpeer . "/hosts/" . $identifier . "/embed";
    $ch_embedded = curl_init();
    curl_setopt($ch_embedded, CURLOPT_URL, $url_live);
    curl_setopt($ch_embedded, CURLOPT_VERBOSE, 0);
    curl_setopt($ch_embedded, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch_embedded, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch_embedded, CURLOPT_USERPWD, $credential);
    curl_setopt($ch_embedded, CURLOPT_SSL_VERIFYPEER, FALSE);
    $dati = curl_exec($ch_embedded);
    $arraydati = json_decode($dati);
    $name = $arraydati->name;
    if ($arraydati->paymentMode=="FREEOFCHARGE")
      $payperview = "0";
    else
      $payperview =  $arraydati->pricePerView;
    $url = $arraydati->url;
    $giorno = $arraydati->eventDate;
    $ora = $arraydati->eventHour . ":" . $arraydati->eventMinute;
    $tempo = $arraydati->duration;
    $ore = floor($tempo / 60);
    $minuti = $tempo % 60;
    $durata = $ore . "h" . $minuti;
    $form['type'] = array('#type' => 'hidden', '#value' => t('modify'));
    $form['identifier'] = array('#type' => 'hidden', '#value' => $identifier);
  }
  else {
    $name = "";
    $payperview = "0";
    $url = "";
    $giorno = "";
    $ora = "";
    $durata = "";
    $form['type'] = array('#type' => 'hidden', '#value' => t('insert'));
  }
  drupal_add_library('system', 'ui.datepicker');
  drupal_add_js('jQuery(document).ready(function(){jQuery( ".pickadate" ).datepicker({
      dateFormat: "dd/mm/yy",
      autoSize: true,
      minDate: 0,
    });});', 'inline');
  drupal_add_js(drupal_get_path('module', 'wimtvpro') . '/jquery/timepicker/jquery.ui.timepicker.js');
  drupal_add_js(drupal_get_path('module', 'wimtvpro') . '/wimtvpro.js');
  drupal_add_css(drupal_get_path('module', 'wimtvpro') . '/jquery/timepicker/jquery.ui.timepicker.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE));
  drupal_add_css(drupal_get_path('module', 'wimtvpro') . '/css/wimtvpro.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE));

  drupal_add_js('jQuery(document).ready(function(){jQuery( ".pickatime" ).timepicker({  defaultTime:"00:00"  });});', 'inline');
  drupal_add_js('jQuery(document).ready(function(){jQuery( ".pickaduration" ).timepicker({   defaultTime:"00h05",showPeriodLabels: false,timeSeparator: "h", });});', 'inline');

  $form['htmltag'] = array(
  '#markup' => variable_get('htmltag', l(t("Return list event"), "admin/config/wimtvpro/wimlive"))
  );


$form['name'] = array(
'#type' => 'textfield',
'#title' => t('Name'),
'#description' => t('Name of the event'),
'#default_value' => variable_get('name', $name),
'#size' => 100,
'#maxlength' => 200,
'#required' => TRUE,
);

$form['payperview'] = array(
'#type' => 'textfield',
'#title' => t('Set the access event'),
'#description' => t('0 as free of charge or you can decide the price of access for each viewer (in &euro;).'),
'#default_value' => variable_get('payperview', $payperview),
'#size' => 10,
'#maxlength' => 5,
'#required' => TRUE,
);



$form['Url'] = array(
'#type' => 'textfield',
'#title' => t('Url'),
'#description' => t('URL through which the streaming can be done. <b class="createUrl"> CREATE YOUR URL </b><b id="' . variable_get("userWimtv") . '" class="removeUrl"> REMOVE YOUR URL </b><br/><div class="passwordUrlLive"> Password Live is missing, insert a password for live streaming: <input type="password" id="passwordLive" /> <b class="createPass">Save</b></div>'),
'#default_value' =>  variable_get('payperview', $url),
'#size' => 100,
'#maxlength' => 800,
'#required' => TRUE,
);


$form['Public'] = array(
    '#type' => 'radios',
    '#title' => t('Event Public or Private'),
    '#maxlength' => 5,
    '#options' => array( 'true' => 'Public', 'false' => 'Private'),
    '#required' => TRUE,
    '#default_value' => variable_get('Public', 'true'),
);


$form['Giorno'] = array(
    '#type' => 'textfield',
    '#title' => t('Date'),
    '#description' => t('Date of the event mm/dd/yy'),
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array('class' => array('pickadate')),
    '#required' => TRUE,
    '#default_value' => $giorno,
);

$form['Ora'] = array(
    '#type' => 'textfield',
    '#title' => t('Start time'),
    '#description' => t('We recommend applying a tolerance on the advance to facilitate payment transactions to the spectators.'),
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array('class' => array('pickatime')),
    '#required' => TRUE,
    '#default_value' => $ora,

);


$form['Duration'] = array(
'#type' => 'textfield',
'#title' => t('Duration'),
'#description' => t('Event duration.'),
'#default_value' => $durata,
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array('class' => array('pickaduration')),
     '#required' => TRUE,
);
if ($type=="modify") {
$form['submit'] = array(
'#type' => 'submit',
'#value' => t('Edit'),
);
}
else {
$form['submit'] = array(
'#type' => 'submit',
'#value' => t('Add'),
);
}
$form['#validate'][] = 'wimtvpro_wimlive_validate';
$form['#submit'][] = 'wimtvpro_wimlive_submit';
return $form;

}

function wimtvpro_set_year_range($form_element) {
  $form_element['year']['#options'] = drupal_map_assoc(range(date("Y"), date("Y")+10));
  return $form_element;
}


function wimtvpro_wimlive_validate($form, &$form_state) {
  $name = check_plain($_POST['name']);
  $payperview = check_plain($_POST['payperview']);
  $public = check_plain($_POST['Public']);

  if ($payperview=="0")
    $typemode = "FREEOFCHARGE";
  else
    $typemode = "PAYPERVIEW&pricePerView=" . $payperview . "&ccy=EUR";

  $url = check_plain($_POST['Url']);
  if ($_POST['Giorno']!="") {
    $giorno = check_plain($_POST['Giorno']);
  }
  else
    $giorno = "";
  if ($_POST['Ora']!="") {
    $ora = explode(":", check_plain($_POST['Ora']));
  }
  else {
    $ora[0] = "";
    $ora[1] = "";
  }
  if ($_POST['Duration']!="") {
    $separe_duration = explode("h", check_plain($_POST['Duration']));
    $duration = ($separe_duration[0] * 60) + $separe_duration[1];
  }
  else {
    $duration = 0;
  }
  $userpeer = variable_get("userWimtv");
  //url-ify the data for the POST
  $fields_string = "name=" . $name . "&url=" . $url . "&eventDate=" . $giorno . "&paymentMode=" . $typemode;
  $fields_string .= "&eventHour=" . $ora[0] . "&eventMinute=" . $ora[1] . "&duration=" . $duration . "&durationUnit=Minute&publicEvent=" . $public;
  $credential = variable_get("userWimtv") . ":" . variable_get("passWimtv");
  $url_live = variable_get("basePathWimtv") . "liveStream/" . $userpeer . "/" . $userpeer . "/hosts";
  if ($_POST['type']=="modify")  $url_live .= "/" . $_POST['identifier'];

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url_live);
  curl_setopt($ch, CURLOPT_USERPWD, $credential);
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  $response = curl_exec($ch);
  curl_close($ch);
  if ($response!="") {
    $message = json_decode($response);
    $result = $message->{"result"};
    if ($result=="SUCCESS") {
      //drupal_set_message("Insert event successfully");
    }
    else {
      $formset_error = "";
      foreach ($message->{"messages"} as $key => $value) {
        if ($value->message!="")
          $formset_error .= $value->field . "=" . $value->message;
      }
      form_set_error("", check_plain($formset_error));
    }
  }
}

function wimtvpro_wimlive_submit($form, &$form_state) {
  drupal_set_message(t("Insert event successfully"));
}

//View event into public page
function wimtvpro_live_public() {
  $output = wimtvpro_elencoLive("0", "video");
  $output .= '<br/><b>UPCOMING EVENTS</b>';
  $output .= "<ul>" . wimtvpro_elencoLive("prev", "list") . "</ul>";
  return $output;
}

//List your future live event
function wimtvpro_elencoLive($number, $type) {
  $userpeer = variable_get("userWimtv");
  $url_live_select = variable_get("basePathWimtv") . "liveStream/" . $userpeer . "/" . $userpeer . "/hosts";
  $credential = variable_get("userWimtv") . ":" . variable_get("passWimtv");
  $ch_select = curl_init();
  curl_setopt($ch_select, CURLOPT_URL, $url_live_select);
  curl_setopt($ch_select, CURLOPT_VERBOSE, 0);
  curl_setopt($ch_select, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch_select, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch_select, CURLOPT_USERPWD, $credential);
  curl_setopt($ch_select, CURLOPT_SSL_VERIFYPEER, FALSE);
  $json  =curl_exec($ch_select);
  $arrayjson_live = json_decode($json);
  $count = -1;
  $output = "";
  if ($arrayjson_live ) {
    foreach ($arrayjson_live->hosts as $key => $value) {
      $count ++;
      $name = $value -> name;
      if (isset($value -> url))
        $url =  $value -> url;
      else
        $url = "";
      $day =  $value -> eventDate;
      $payment_mode =  $value -> paymentMode;
      if ($payment_mode=="FREEOFCHARGE") $payment_mode="Free";
      else $payment_mode= pricePerView . " &euro;";
      $durata =  $value->duration . " " . $value -> durationUnit;
      $identifier = $value -> identifier;
      $url_live_embedded = variable_get("basePathWimtv") . "liveStream/" . $userpeer . "/" . $userpeer . "/hosts/" . $identifier . "/embed";
      $ch_embedded = curl_init();
      $header[] = "Accept: text/xml,application/xml,application/xhtml+xml,";
      curl_setopt($ch_embedded, CURLOPT_URL, $url_live_embedded);
      curl_setopt($ch_embedded, CURLOPT_VERBOSE, 0);
      curl_setopt($ch_embedded, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch_embedded, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch_embedded, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch_embedded, CURLOPT_USERPWD, $credential);
      curl_setopt($ch_embedded, CURLOPT_SSL_VERIFYPEER, FALSE);
      $embedded_iframe = curl_exec($ch_embedded);
      $embedded_code = '<textarea readonly="readonly" onclick="this.focus(); this.select();">' . $embedded_iframe . '</textarea>';
      if ($type=="table") {
        $output .="<tr>
        <td>" . $name . "</td>
        <td>" . $payment_mode . "</td>
        <td>" . $url . "</td>
        <td>" . $day . "<br/>" . $durata . "</td>
        <td>" . $embedded_code . "</td>
        <td>" . l(t("Edit"), "admin/config/wimtvpro/wimlive/modify/" . $identifier ) . " | " . l(t("Delete"), "admin/config/wimtvpro/wimlive/delete/" . $identifier ) . "</td>
        </tr>";
      }
      elseif ($type=="list") {
        if (($number=="prev") && ($count==0)) $output .= "";
        elseif (($number=="prev") && ($count>0)) $output .="<li><b>" . $name . "</b> " . $payment_mode . " - " . $day . " - " . $durata . "</li>";
        else $output .="<li><b>" . $name . "</b> " . $payment_mode . " - " . $day . " - " . $durata . "</li>";
      }
      else {
        $name = "<b>" . $name . "</b>";
        $day =  "Begins to " . $day;
        $output = $name . "<br/>";
        $output .= $day . "<br/>" . $durata . "<br/>";
        $output .= $embedded_iframe;
      }
      if (($number=="0") && ($count==0)) break;
    }
  }
  if ($count<0) {
    $output = t("Aren't Event Live");
  }
  return $output;
}